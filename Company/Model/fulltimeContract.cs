﻿using System.Runtime.Serialization;

namespace Company.Model
{
    [DataContract]
    public class FulltimeContract : Contract
    {
        private static readonly double _defaultSalary = 3600;

        public static readonly double DefaultOvertime = 0;
        public bool IsDefaultOvertime
        {
            get
            {
                if (DefaultOvertime == Overtime)
                    return true;
                return false;
            }
        }

        [DataMember]
        public double Overtime { get; private set; }
        
        public FulltimeContract()
            : base(_defaultSalary, _defaultSalary)
        {
            Overtime = 0;
        }

        public FulltimeContract(double salary = 3600, double overtime = 0)
            : base(_defaultSalary, salary)
        {
            Overtime = overtime;
        }

        public override double Salary
        {
            get
            {
                return base.Salary + (Overtime * base.Salary / 60);
            }
        }

        public double baseSalary()
        {
            return base.Salary;
        }

        public void changeOvertime(double overtime)
        {
            Overtime = overtime;
        }
    }
}
