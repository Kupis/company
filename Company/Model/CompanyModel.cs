﻿using System;
using System.Collections.Generic;

namespace Company.Model
{
    public class CompanyModel
    {
        private List<Employee> _employees = new List<Employee>();

        public void LoadEmployes()
        {
            _employees = IO.LoadEmployees();

            foreach (Employee employee in _employees)
                OnEmployeeChanged(employee, EmployeeChangedEventType.Add, 0);
        }

        public void addEmployee(string name, string surname, Contract contract, string salaryString, string overtimeString)
        {
            Employee employee = new Employee(name, surname, contract);

            double salary;
            if (salaryString != "" && Double.TryParse(salaryString, out salary))
                employee.Contract.changeSalary(salary);

            if (employee.Contract is FulltimeContract)
            {
                double overtime;
                if (overtimeString != "" && Double.TryParse(overtimeString, out overtime))
                {
                    FulltimeContract fulltimeContract = employee.Contract as FulltimeContract;
                    fulltimeContract.changeOvertime(overtime);
                }
            }

            _employees.Add(employee);
            IO.SaveEmployee(_employees);
            OnEmployeeChanged(employee, EmployeeChangedEventType.Add, _employees.Count);
        }

        public void editEmployee(int index, string name, string surname, Contract contractType, string salaryString, string overtimeString)
        {
            _employees[index] = new Employee(name, surname, contractType);

            double salary;
            if (salaryString != "" && Double.TryParse(salaryString, out salary))
                _employees[index].Contract.changeSalary(salary);

            if (_employees[index].Contract is FulltimeContract)
            {
                double overtime;
                if (overtimeString != "" && Double.TryParse(overtimeString, out overtime))
                {
                    FulltimeContract fulltimeContract = _employees[index].Contract as FulltimeContract;
                    fulltimeContract.changeOvertime(overtime);
                }
            }

            IO.SaveEmployee(_employees);
            OnEmployeeChanged(_employees[index], EmployeeChangedEventType.Edit, index);
        }

        public void deleteEmployee(int index)
        {
            OnEmployeeChanged(_employees[index], EmployeeChangedEventType.Delete, index);
            _employees.RemoveAt(index);
            IO.SaveEmployee(_employees);
        }

        public event EventHandler<EmployeeChangedEventArgs> EmployeeChanged;
        private void OnEmployeeChanged(Employee employee, EmployeeChangedEventType type, int index)
        {
            EmployeeChanged?.Invoke(this, new EmployeeChangedEventArgs(employee, type, index));
        }
    }
}
