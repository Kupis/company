﻿using System;

namespace Company.Model
{
    public class EmployeeChangedEventArgs : EventArgs
    {
        public Employee Employee { get; private set; }
        public EmployeeChangedEventType Type { get; private set; }
        public int EmployeeIndex { get; private set; }

        public EmployeeChangedEventArgs(Employee employee, EmployeeChangedEventType type, int index)
        {
            Employee = employee;
            Type = type;
            EmployeeIndex = index;
        }
    }
}
