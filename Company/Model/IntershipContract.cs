﻿namespace Company.Model
{
    public class IntershipContract : Contract
    {
        private static readonly double _defaultSalary = 1500;

        public IntershipContract()
            : base(_defaultSalary, _defaultSalary)
        {

        }

        public IntershipContract(double salary)
            : base(_defaultSalary, salary)
        {

        }
    }
}
