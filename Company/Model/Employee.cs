﻿using System.Runtime.Serialization;

namespace Company.Model
{
    [KnownType(typeof(Contract))]
    [KnownType(typeof(IntershipContract))]
    [KnownType(typeof(FulltimeContract))]
    [DataContract]
    public class Employee
    {
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public string Surname { get; private set; }
        [DataMember]
        public Contract Contract { get; private set; }

        public Employee(string name, string surname, Contract contract)
        {
            Name = name;
            Surname = surname;
            Contract = contract;
        }

        public string ContractName { get { return Contract.ContractName; } }

        public void changeContract(Contract contract)
        {
            Contract = contract;
        }

        public double Salary { get { return Contract.Salary; } }

        public override string ToString()
        {
            return Name + " " + Surname + " " + Salary.ToString();
        }
    }
}
