﻿using System.ComponentModel;

namespace Company.Model
{
    public enum ContractType
    {
        Intership,
        Fulltime
    }

    public enum EmployeeChangedEventType
    {
        Add,
        Edit,
        Delete
    }

    public enum Languages
    {
        English,
        Polish
    }
}
