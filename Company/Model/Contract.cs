﻿using System.Runtime.Serialization;

namespace Company.Model
{
    [DataContract]
    public abstract class Contract
    {
        [DataMember]
        private double _salary;
        public virtual double Salary { get { return _salary; } }

        [DataMember]
        public double DefaultSalary { get; private set; }
        public bool IsDefaultSalary
        {
            get
            {
                if (_salary == DefaultSalary)
                    return true;
                return false;
            }
        }

        [DataMember]
        public string ContractName { get; private set; }

        public Contract(double defaultSalary, double salary)
        {
            DefaultSalary = defaultSalary;
            _salary = salary;

            if (this is IntershipContract)
                ContractName = ContractType.Intership.ToString();
            else if (this is FulltimeContract)
                ContractName = ContractType.Fulltime.ToString();
            else
                ContractName = "Unknown";
        }

        public void changeSalary (double salary)
        {
            _salary = salary;
        }
    }
}
