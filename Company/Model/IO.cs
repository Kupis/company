﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Windows;

namespace Company.Model
{
    public static class IO
    {
        private static string path = @"Employee.xml";

        public static void SaveEmployee(List<Employee> employees)
        {
            if (!File.Exists(path))
                File.Create(path).Close();

            File.Delete(path);

            using (Stream outputStream = File.OpenWrite(path))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<Employee>));
                serializer.WriteObject(outputStream, employees);
            }
        }

        public static List<Employee> LoadEmployees()
        {
            if (!File.Exists(path))
                DataSeed();

            List<Employee> employees;
            try
            {
                using (Stream stream = File.OpenRead(path))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(List<Employee>));
                    employees = serializer.ReadObject(stream) as List<Employee>;
                }
                return employees;
            }
            catch (System.Xml.XmlException)
            {
                MessageBox.Show(lang.lang.xmlError);
                return new List<Employee>();
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                MessageBox.Show(lang.lang.xmlError);
                return new List<Employee>();
            }
        }

        public static void DataSeed()
        {
            List<Employee> dataSeedList = new List<Employee>()
            {
                {new Employee("Seba", "Nowak", new IntershipContract()) },
                {new Employee("Janusz", "Kowalski", new FulltimeContract()) },
                {new Employee("Mietek", "Piwerko", new FulltimeContract(2300, 30)) },
                {new Employee("Grażynka", "Solaris", new IntershipContract(1800)) }
            };

            SaveEmployee(dataSeedList);
        }
    }
}
