﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Company.ViewModel
{
    static class ContractHelper
    {
        public static Model.Contract GetContractFromEnumFactory(Model.ContractType contractType)
        {
            if (contractType == Model.ContractType.Intership)
                return new Model.IntershipContract();
            else if (contractType == Model.ContractType.Fulltime)
                return new Model.FulltimeContract();
            else
                throw new Exception();
        }

        public static Model.ContractType GetEnumFromContract(Model.Contract contract)
        {
            if (contract is Model.IntershipContract)
                return Model.ContractType.Intership;
            else if (contract is Model.FulltimeContract)
                return Model.ContractType.Fulltime;
            else
                throw new Exception();
        }
    }
}
