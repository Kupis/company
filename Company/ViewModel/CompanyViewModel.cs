﻿using Company.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;

namespace Company.ViewModel
{
    class CompanyViewModel : INotifyPropertyChanged
    {
        private CompanyModel _model = new CompanyModel();

        private ObservableCollection<Employee> _employees = new ObservableCollection<Employee>();
        public INotifyPropertyChanged Employees { get { return _employees; } }

        public CompanyViewModel()
        {
            _model.EmployeeChanged += _model_EmployeeChanged;
        }

        public void loadEmployes()
        {
            _model.LoadEmployes();
        }

        private void _model_EmployeeChanged(object sender, EmployeeChangedEventArgs e)
        {
            if (e.Type == EmployeeChangedEventType.Add)
            {
                _employees.Add(e.Employee);
                OnPropertyChanged("Employees");
            }
            else if (e.Type == EmployeeChangedEventType.Edit)
            {
                _employees[e.EmployeeIndex] = e.Employee;
                OnPropertyChanged("Employees");
            }
            else if (e.Type == EmployeeChangedEventType.Delete)
            {
                _employees.Remove(e.Employee);
                OnPropertyChanged("Employees");
            }
        }

        public void addEmployee()
        {
            View.SelectedEmployee addWindow = new View.SelectedEmployee(lang.lang.addTitle, true);
            addWindow.ShowDialog();
            if (addWindow.DialogResult.HasValue && addWindow.DialogResult.Value)
            {
                var contractType = ContractHelper.GetContractFromEnumFactory((ContractType)addWindow.comboBox.SelectedItem);
                _model.addEmployee(addWindow.nameTextBox.Text, addWindow.surnameTextBox.Text, contractType, addWindow.salaryTextBox.Text, addWindow.overtimeTextBox.Text);
            }
        }

        public void editEmployee(int index)
        {
            Employee employee = _employees[index];
            ContractType type = ContractHelper.GetEnumFromContract(employee.Contract);

            View.SelectedEmployee editWindow = new View.SelectedEmployee(lang.lang.editTitle);

            double salary = employee.Salary;
            string overtime = "";
            if (employee.Contract is FulltimeContract)
            {
                FulltimeContract contract = employee.Contract as FulltimeContract;
                overtime = contract.Overtime.ToString();

                salary = contract.baseSalary();
            }

            editWindow.SendDataToWindow(employee.Name, employee.Surname, type, salary, overtime);

            editWindow.ShowDialog();
            if (editWindow.DialogResult.HasValue && editWindow.DialogResult.Value)
            {
                var contractType = ContractHelper.GetContractFromEnumFactory((Model.ContractType)editWindow.comboBox.SelectedItem);
                _model.editEmployee(index, editWindow.nameTextBox.Text, editWindow.surnameTextBox.Text, contractType, editWindow.salaryTextBox.Text, editWindow.overtimeTextBox.Text);
            }
        }

        public void deleteEmployee(int index)
        {
            _model.deleteEmployee(index);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
