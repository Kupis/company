﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Company.ViewModel
{
    class OvertimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Model.FulltimeContract)
            {
                Model.FulltimeContract contract = value as Model.FulltimeContract;
                return contract.Overtime.ToString();
            }
            else
                return "not applicable";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class DefaultSalaryConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
                return new SolidColorBrush(Colors.Green);
            return new SolidColorBrush(Colors.DarkBlue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class DefaultOvertimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Model.FulltimeContract)
            {
                Model.FulltimeContract contract = value as Model.FulltimeContract;
                if (contract.IsDefaultOvertime)
                    return new SolidColorBrush(Colors.Green);
                return new SolidColorBrush(Colors.DarkBlue);
            }
            return new SolidColorBrush(Colors.DarkBlue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class ContractTypeLanguageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                Model.ContractType type = (Model.ContractType) value;
                if (type == Model.ContractType.Intership)
                    return lang.lang.intership;
                else if (type == Model.ContractType.Fulltime)
                    return lang.lang.fulltime;
                else
                    return value;
            }
            catch (Exception)
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class ListContractTypeLanguageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.ToString() == Model.ContractType.Intership.ToString())
                return lang.lang.intership;
            else if (value.ToString() == Model.ContractType.Fulltime.ToString())
                return lang.lang.fulltime;
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class EmployeesCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return lang.lang.employees + ": " + value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
