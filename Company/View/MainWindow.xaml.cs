﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;

namespace Company.View
{
    /// <summary>
    /// Interaction logic for mainWindow.xaml
    /// </summary>
    public partial class mainWindow : Window
    {
        ViewModel.CompanyViewModel viewModel;

        public mainWindow()
        {
            InitializeComponent();

            languageComboBox.ItemsSource = Enum.GetValues(typeof(Model.Languages)).Cast<Model.Languages>();
            if (Thread.CurrentThread.CurrentUICulture.Name.StartsWith("pl"))
                languageComboBox.SelectedItem = Model.Languages.Polish;
            else
                languageComboBox.SelectedItem = Model.Languages.English;

            viewModel = FindResource("viewModel") as ViewModel.CompanyViewModel;
            viewModel.loadEmployes();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            viewModel.addEmployee();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == -1)
            {
                MessageBox.Show(lang.lang.notSelectedEmployee);
                return;
            }
            else
            {
                viewModel.editEmployee(listView.SelectedIndex);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == -1)
            {
                MessageBox.Show(lang.lang.notSelectedEmployee);
                return;
            }
            else
            {
                viewModel.deleteEmployee(listView.SelectedIndex);
            }
        }

        private void listView_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (listView.SelectedIndex == -1)
            {
                MessageBox.Show(lang.lang.notSelectedEmployee);
                return;
            }
            else
            {
                viewModel.editEmployee(listView.SelectedIndex);
            }
        }

        private void languageComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if ((Model.Languages)languageComboBox.SelectedItem == Model.Languages.English)
            {
                if (!Thread.CurrentThread.CurrentUICulture.Name.StartsWith("en"))
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
                    mainWindow win = new mainWindow();
                    win.Show();
                    this.Close();
                }
            }
            else if ((Model.Languages)languageComboBox.SelectedItem == Model.Languages.Polish)
            {
                if (!Thread.CurrentThread.CurrentUICulture.Name.StartsWith("pl"))
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("pl");
                    mainWindow win = new mainWindow();
                    win.Show();
                    this.Close();
                }
            }
        }
    }
}
