﻿using System;
using System.Linq;
using System.Windows;

namespace Company.View
{
    /// <summary>
    /// Interaction logic for selectedEmployee.xaml
    /// </summary>
    public partial class SelectedEmployee : Window
    {
        private bool _loadDefaultValues;

        public SelectedEmployee(string windowTittle, bool loadDefaultValues = false)
        {
            InitializeComponent();

            this.Title = windowTittle;
            _loadDefaultValues = loadDefaultValues;

            comboBox.ItemsSource = Enum.GetValues(typeof(Model.ContractType)).Cast<Model.ContractType>();
            comboBox.SelectedIndex = 0;

            setDefaultValue();
        }

        private void setDefaultValue()
        {
            if (_loadDefaultValues)
            {
                Model.Contract contract = ViewModel.ContractHelper.GetContractFromEnumFactory((Model.ContractType)comboBox.SelectedItem);
                salaryTextBox.Text = contract.DefaultSalary.ToString();

                if (contract is Model.FulltimeContract)
                {
                    overtimeTextBox.Text = Model.FulltimeContract.DefaultOvertime.ToString();
                }
            }
        }

        public void SendDataToWindow(string name, string surname, Model.ContractType contract, double salary, string overtime)
        {
            nameTextBox.Text = name;
            surnameTextBox.Text = surname;
            comboBox.SelectedItem = contract;

            salaryTextBox.Text = salary.ToString();
            overtimeTextBox.Text = overtime;
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            double salary;
            double overtime;

            if (String.IsNullOrWhiteSpace(nameTextBox.Text))
                MessageBox.Show(lang.lang.emptyName);
            else if (String.IsNullOrWhiteSpace(surnameTextBox.Text))
                MessageBox.Show(lang.lang.emptySurname);
            else if (Double.TryParse(salaryTextBox.Text, out salary) && salary < 0)
                MessageBox.Show(lang.lang.salaryNegative);
            else if (Double.TryParse(overtimeTextBox.Text, out overtime) && overtime < 0)
                MessageBox.Show(lang.lang.overtimeNegative);
            else
            {
                DialogResult = true;
                this.Close();
            }
        }

        private void comboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                Model.ContractType contractType = (Model.ContractType)Enum.Parse(typeof(Model.ContractType), comboBox.SelectedItem.ToString());

                if (contractType == Model.ContractType.Intership)
                {
                    overtimeGrid.Visibility = Visibility.Collapsed;
                }
                else if (contractType == Model.ContractType.Fulltime)
                {
                    overtimeGrid.Visibility = Visibility.Visible;
                }

                setDefaultValue();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);

                overtimeGrid.Visibility = Visibility.Visible;
            }
        }
    }
}
