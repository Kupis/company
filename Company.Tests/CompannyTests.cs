﻿using NUnit.Framework;

namespace Company.Tests
{
    public class CompannyTests
    {
        [TestCase(600, 30, 900)]
        [TestCase(1200, 33, 1860)]
        [TestCase(1221, 31, 1851.85)]
        [TestCase(231, 21, 311.85)]
        [TestCase(6455, 3, 6777.75)]
        public void FulltimeContract_SalaryDependingOnOvertime_Salary(double basicSalary, double overtime, double expectedSalary)
        {
            Model.FulltimeContract contract = new Model.FulltimeContract(basicSalary, overtime);
            double salary = contract.Salary;
            Assert.AreEqual(expectedSalary, salary);
        }

        [Test]
        public void FulltimeContract_ChangeSalary_Salary()
        {
            Model.FulltimeContract contract = new Model.FulltimeContract(600);
            contract.changeSalary(1600);
            Assert.AreEqual(1600, contract.Salary);
        }

        [Test]
        public void Intership_Changesalary_Salary()
        {
            Model.IntershipContract contract = new Model.IntershipContract(600);
            contract.changeSalary(1600);
            Assert.AreEqual(1600, contract.Salary);
        }

        [Test]
        public void FulltimeContract_ChangeOvertime_Overtime()
        {
            Model.FulltimeContract contract = new Model.FulltimeContract(600, 10);
            contract.changeOvertime(30);
            Assert.AreEqual(30, contract.Overtime);
        }

        [Test]
        public void CompanyModel_OnEmployeeChangedEventIsCalledWhenAddingEmployee()
        {
            Model.CompanyModel model = new Model.CompanyModel();

            bool wasEventCalled = false;
            model.EmployeeChanged += (sender, args) => wasEventCalled = true;

            model.addEmployee("testName", "testSurname", new Model.IntershipContract(), "600", "30");

            Assert.IsTrue(wasEventCalled);
        }

        [Test]
        public void CompanyModel_OnEmployeeChangedEventIsCalledWhenEditingEmployee()
        {
            Model.CompanyModel model = new Model.CompanyModel();
            model.addEmployee("testName", "testSurname", new Model.IntershipContract(), "600", "30");

            bool wasEventCalled = false;
            model.EmployeeChanged += (sender, args) => wasEventCalled = true;

            model.editEmployee(0, "newTestName", "newTestSurname", new Model.FulltimeContract(), "900", "60");

            Assert.IsTrue(wasEventCalled);
        }

        [Test]
        public void CompanyModel_OnEmployeeChangedEventIsCalledWhenDeletinggEmployee()
        {
            Model.CompanyModel model = new Model.CompanyModel();
            model.addEmployee("testName", "testSurname", new Model.IntershipContract(), "600", "30");

            bool wasEventCalled = false;
            model.EmployeeChanged += (sender, args) => wasEventCalled = true;

            model.deleteEmployee(0);

            Assert.IsTrue(wasEventCalled);
        }
    }
}
