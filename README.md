# Company

WPF .NET company management application.

## Save options

* Master branch use data contract to serialize employees to employees.xml file in the same path where .exe is,
* database branch use MySQL database connection. Database diagram is created by [UNSUPPORTED with commit 8aea4e58],
* csv branch save employees to employees.csv file [UNSUPPORTED with commit 8aea4e58].

## Assumptions

Create basic employee management application for a company.

### Employee

Implement the class representing the employee.

Each employee have:
* Name,
* Surname,
* Contract.

Employee class provides operations:
* initial constructor with the name and surname given int  the arguments with the default contract of the intership,
* method to change contract assigned to the employee,
* method that returns the amount of the employee's salary  depending on the contract being signed,
* overloaded ToString() method containing a string with name, surname and amount of the employee's salary.

### Contract

Each contract is represented by an object that publicly publishes the following operations:
* method that returns the amount of salary paid out under a given contract,
* constructor that allows you to initialize all component fields of an object,
* default constructor.

Currently, two types of contracts are signed at company:
* Intership - monthly rate (1500 PLN by default),
* Fulltime employment - monthly rate (3600 PLN by default), overtime (default 0).

Wee assume that in in the future there will be other types of contracts differing in the way of calculating salary. The contract class is responsible for calculating the salary, which provides a public method Salary() that returns salary. Therefore, adding a new type of contract is limited to implementing only one class, without the need to modify existing classes.

### Salary

The amount of the salary is determined for each of these contracts differently:
* The intership's salary is always equal to the monthly rate,
* The fulltime employment = monthly rate + overtime * (monthly / 60).

### Requirements

* C# language,
* Simple graphic interface in WPF - displaying he list of employees along with their salaries, adding, removing employees and editing the contract,
* Use one of this patterns: MVC / MVP / MVVM,
* A few simple unit tests.

## Author

* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details